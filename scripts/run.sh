#!/bin/sh

VIRT_DIR=/home/martins/Projects/virtualization
VIRT_IMAGE_DIR=images
VIRT_IMAGE=windows10.img
VIRT_SYSTEM_DIR=systems
VIRT_SYSTEM=windows10.iso

GPU_VGA_HOST=01:00.0
GPU_AUDIO_HOST=01:00.1

KBD_VENDOR=04d9
KBD_ID=0141
MOUSE_VENDOR=046d
MOUSE_ID=c06d

#use alsa driver for host audio
export QEMU_AUDIO_DRV=alsa

#create the initial image
if [ ! -f ${VIRT_DIR}/${VIRT_IMAGE_DIR}/${VIRT_IMAGE} ]; then
	qemu-img create \
		`#only allocate size when the guest needs it` \
		-f qcow2 \
		`#improve cache speed` \
		-o lazy_refcounts=on \
		${VIRT_DIR}/${VIRT_IMAGE_DIR}/${VIRT_IMAGE} 64G
fi

#copy the OVMF variables to the temporary storage
if [ ! -f /tmp/OVMF_VARS.fd ]; then
	cp /usr/share/ovmf/x64/OVMF_VARS.fd /tmp/
fi

qemu-system-x86_64 \
	`#enable the kvm hypervisor` \
	-enable-kvm \
	`#use kvm on guest` \
	-machine type=pc,accel=kvm \
	`#set the memory` \
	-m 8192 \
	`#match cpu to the one of the host, enable hyper-v enlightment for Windows guests` \
	-cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time \
	`#assign more cpus` \
	-smp cores=4 \
	`#std supports up to 2560x1600 without guest drivers` \
	-vga std \
	`#using HD audio for the guest` \
	-soundhw hda \
	`#use virtio drivers passthrough for networking` \
	-netdev user,id=net0 \
	-device virtio-net,netdev=net0 \
	`#attach the respective PCI devices` \
	-device vfio-pci,host=${GPU_VGA_HOST},multifunction=on \
	-device vfio-pci,host=${GPU_AUDIO_HOST} \
	`#passthrough mouse and kbd because of the terrible lag` \
	-device nec-usb-xhci,id=xhci0 \
	-device usb-host,bus=xhci0.0,vendorid=0x$KBD_VENDOR,productid=0x$KBD_ID \
	-device nec-usb-xhci,id=xhci1 \
	-device usb-host,bus=xhci1.0,vendorid=0x$MOUSE_VENDOR,productid=0x$MOUSE_ID \
	`#OVMF is an open-source UEFI firmware for QEMU virtual machines which provides better performance which allows PCI passthrough` \
	-drive file=/usr/share/ovmf/x64/OVMF_CODE.fd,if=pflash,format=raw,readonly \
	-drive file=/tmp/OVMF_VARS.fd,if=pflash,format=raw \
	`#use virtio drivers instead of IDE to improve the performance. l2_cache_size = disk_size * 8 / cluster_size. The default cluster size is 64K` \
	-drive file=${VIRT_DIR}/${VIRT_IMAGE_DIR}/${VIRT_IMAGE},index=0,media=disk,format=qcow2,l2-cache-size=4M,if=virtio \
	-drive file=${VIRT_DIR}/${VIRT_SYSTEM_DIR}/${VIRT_SYSTEM},index=1,media=cdrom \
	-drive file=/usr/share/virtio/virtio-win.iso,index=2,media=cdrom \
