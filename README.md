# Virtualization

Virtualization scripts and configs for Arch Linux hosts using KVM, QEMU, VFIO and OVMF.

## Hardware

- OS: Arch Linux running `linux-vfio` kernel.
- CPU: Intel i5 6600K.
- GPU: Gigabyte Radeon RX 460.
- Mobo: Asus Z170i.
- Storage: Samsung 850 EVO 500GB.
- Memory: 16GB Corsair DDR4.

## Software

- `linux-vfio`
- `qemu`
- `virtio-win`
- `ovmf`

## Notes

- You can easy simlink the config files using `stow -t / boot mkinitcpio` and then `mkinitcpio -p linux-vfio`. Note that the config files provided and the default ones are not the same, e.g. the `mkinitcpio-vfio.conf` file.
- Newer Arch Linux kernels have the modules included by default, so there is no need to use `linux-vfio`.
- Use `virtio` drivers for both block devices and network. For example, the ping went down from 250 to 50.
- Mouse and keyboard passthrough solved the terrible lag that was present in the emulation mode.
- Make sure virtualization is supported and enabled in your firmware (UEFI). The option was hidden in a submenu in my case.
- Be patient as it took more than 10 minutes for the guest to recognize the GPU, displays, sound driver, to install updates, etc.
